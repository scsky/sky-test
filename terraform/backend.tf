terraform {
  backend "s3" {
    bucket = "skyonboarding-node-aws-jenkins-terraform"
    key = "skyonboarding-node-aws-jenkins-terraform.tfstate"
    region = "eu-west-1"
  }
}